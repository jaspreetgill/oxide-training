(function($, window, document, undefined) {
  /**
  * Banner image
  **/
    Drupal.behaviors.objectFit = {
      attach: function(context, settings) {
        // For browsers that don't support CSS object-fit (including IE)
        $(".content-area-wrapper .field-name-field-banner-image .field-items").not(".img-cover-processed").each(function() {
            var $img = $(this).find("img").first();
            var $wrapper = $('<div class="img-cover-wrapper"></div>');
            $wrapper.css("background-image", "url(" + $img.attr("src") + ")");
            $(this).wrapAll($wrapper);
            $('.field-name-field-banner-image').addClass("img-cover-processed");
          });
      }
    };
  })(jQuery, window, document);
