(function($, window, document, undefined) {

  /**
  * Collapsible Menu
  */
  Drupal.behaviors.collapsibleMenu = {
    attach: function(context, settings) {

      // menu item that has children
      var expandedItem = $('.block-menu-block .menu__item.is-expanded');

      // Exclude extra menus on page
      $('.extra-menu-class').closest('section').addClass('not-collapsible-menu');

      // Create the expand button
      $('.block-menu-block').once('collapsibleMenu', function(){

        // If menu item has child list, then add menu collapser span
        $(expandedItem).has('ul').each(function(index) {
          $(this).children('a').after( '<span class="menu-collapser"></span>' );
          $('.menu-collapser').attr({ tabindex: '0', title: "Press [enter] to open list of contents", role: 'button', "aria-expanded": 'false'});
        });

        // Do the expand on click
        $('.menu-collapser').click(function(e) {
          expandCollapse(this);
        });

        // Do the expand on 'enter'
        $('.menu-collapser').keypress(function(e) {
          if(e.which == 13) {
            expandCollapse(this);
          }
        });

        // Expand menu item onload if it has active-trail class
        $('.block-menu-block .menu__item.is-expanded.is-active-trail > .menu-collapser').each(function() {
          $(this).once('openActiveTree', function(){
            $(this).parent('ul').each(expandCollapse(this));
          });
        });

      });

      // Expand menu collapser
      function expandCollapse(target) {
        $(target).next('ul').slideToggle();
        $(target).parent().toggleClass( 'menu-collapser-collapsed' );
        if ($(target).parent().hasClass("menu-collapser-collapsed")) {
          // It's open
          $(target).attr({
            "aria-expanded": "true",
            "aria-live": "assertive"
            });
        } else {
          $(target).attr("aria-expanded", "false");
        }
      }

    }
  };

})(jQuery, window, document);