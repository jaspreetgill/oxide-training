(function($, Drupal, window, document, undefined) {
  // To understand behaviors, see https://drupal.org/node/756722#behaviors

  /**
   * Inline SVG converter
   */
   // Do the injection for the selected Nodelist
  Drupal.behaviors.injectSvg = {
    attach: function(context, settings) {
      new SVGInjector().inject(document.querySelectorAll('img[src$=".svg"]'))
    }
  }

})(jQuery, Drupal, this, this.document)
