(function($, Drupal, window, document, undefined) {
  // To understand behaviors, see https://drupal.org/node/756722#behaviors

  /**
  * Inline SVG converter
  */
  // Do the injection for the selected Nodelist
  Drupal.behaviors.injectSvg = {
    attach: function(context, settings) {
      new SVGInjector().inject(document.querySelectorAll('img[src$=".svg"]'))
    }
  }

  /**
   * Add expand/collapse to Accordion
   */
   Drupal.behaviors.Accordion = {
    attach: function(context, settings) {
      // Create the collapse button
      $(
        ".collapse-text-fieldset .fieldset-title"
      ).each(function(index) {
        $(this).attr({
          tabindex: "0",
          title: ""
        });
        $(this).attr("aria-expanded", "false");
      });
      // Do the collapse on click
      $(
        ".collapse-text-fieldset .fieldset-title"
      ).click(function(e) {
        expandCollapseAccordion($(this));
      });
      // Do the collapse on 'enter'
      $(
        ".collapse-text-fieldset .fieldset-title"
      ).keypress(function(e) {
        if (e.which == 13) {
          expandCollapseAccordion($(this));
        }
      });

      function expandCollapseAccordion($this) {
        $this
          .parents(".collapse-text-fieldset legend")
          .next(".fieldset-wrapper")
          .slideToggle();
        $this.toggleClass("collapsed");
        if ($this.hasClass("collapsed")) {
          // It's open
          $this.attr("aria-expanded", "true");
        } else {
          $this.attr("aria-expanded", "false");
        }
      }
    }
  };

  /**
   * Toggler open/close animation
   */
   Drupal.behaviors.Toggler = {
    attach: function(context, settings) {
      $('.toggler').append('<span class="hamburger"></span>');
      $('.toggler').click(function() {
        $('.toggler').toggleClass('active');
      });
    }
  };

  /**
  * Banner image
  **/
    Drupal.behaviors.objectFit = {
      attach: function(context, settings) {
        // For browsers that don't support CSS object-fit (including IE)
        $(".content-area-wrapper .field-name-field-banner-image .field-items").not(".img-cover-processed").each(function() {
            var $img = $(this).find("img").first();
            var $wrapper = $('<div class="img-cover-wrapper"></div>');
            $wrapper.css("background-image", "url(" + $img.attr("src") + ")");
            $(this).wrapAll($wrapper);
            $('.field-name-field-banner-image').addClass("img-cover-processed");
          });
      }
    };

})(jQuery, Drupal, this, this.document)
