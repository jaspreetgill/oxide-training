<?php include("inc/header.php") ?>

<div class="content-area-wrapper">
  <?php if ($messages) : ?>
    <div class="content-wrapper">
      <div class="container">
        <?php print $messages; ?>
      </div>
    </div>
  <?php endif; ?>

  <div class="container">
    <?php print render($page['content_top']); ?>
    
    <article class="main" id="main-content">
      <?php print render($title_prefix); ?>
      <?php if (!empty($title)) : ?>
        <h1 class="page-title"><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>

      <?php if ($action_links) : ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>

      <?php if ($tabs['#primary']) : ?>
        <?php print render($tabs); ?>
      <?php endif; ?>

      <div class="content-wrapper">
        <?php print render($page['content']); ?>
      </div>
    </article>

    <?php if ($page['sidebar_first']): ?>
      <aside role="complementary" class="sidebar sidebar_first">
        <?php print render($page['sidebar_first']); ?>
      </aside>
    <?php endif; ?>

    <?php if ($page['sidebar_second']) : ?>
    <aside role="complementary" class="sidebar sidebar_second">
      <?php print render($page['sidebar_second']); ?>
    </aside>
    <?php endif; ?>
  </div>
</div>

<?php include("inc/footer.php") ?>
