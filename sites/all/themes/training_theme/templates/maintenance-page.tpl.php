<!doctype html>
<html class="no-js" lang="">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php print $head_title; ?></title>
    <?php print $head; ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php print $styles; ?>
  </head>
  <body>
    <!--[if lte IE 9]>
      <div class="browserupgrade"><p>You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p></div>
    <![endif]-->
      <div class="page-maintenance ">
         <h1><?php print $title; ?></h1>
        <?php print $content ?>
      </div>
    </div>
  </body>
</html>
