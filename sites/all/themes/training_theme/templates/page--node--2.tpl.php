<?php

/**
 * @file
 * Default theme implementation to display the basic html structure of a single
 * Drupal page.
 *
 * Variables:
 * - $css: An array of CSS files for the current page.
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or 'rtl'.
 * - $rdf_namespaces: All the RDF namespace prefixes used in the HTML document.
 * - $grddl_profile: A GRDDL profile allowing agents to extract the RDF data.
 * - $head_title: A modified version of the page title, for use in the TITLE
 *   tag.
 * - $head_title_array: (array) An associative array containing the string parts
 *   that were used to generate the $head_title variable, already prepared to be
 *   output as TITLE tag. The key/value pairs may contain one or more of the
 *   following, depending on conditions:
 *   - title: The title of the current page, if any.
 *   - name: The name of the site.
 *   - slogan: The slogan of the site, if any, and if there is no title.
 * - $head: Markup for the HEAD section (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $page_top: Initial markup from any modules that have altered the
 *   page. This variable should always be output first, before all other dynamic
 *   content.
 * - $page: The rendered page content.
 * - $page_bottom: Final closing markup from any modules that have altered the
 *   page. This variable should always be output last, after all other dynamic
 *   content.
 * - $classes String of classes that can be used to style contextually through
 *   CSS.
 *
 * @see template_preprocess()
 * @see template_preprocess_html()
 * @see template_process()
 *
 * @ingroup themeable
 */
?><!doctype html>
<html class="no-js" lang="">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php print $head_title; ?></title>
    <?php print $head; ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php print $styles; ?>
    <?php print $scripts; ?>
  </head>
  <body class="<?php print $classes; ?>" <?php print $attributes;?>>
    <div id="skip-link">
      <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
    </div>
    <!--[if lte IE 9]>
      <div class="browserupgrade"><p>You are using an <strong>outdated</strong> browser. Please <a href="https://browsehappy.com/">upgrade your browser</a> to improve your experience and security.</p></div>
    <![endif]-->
    <?php print $page_top; ?>

    <header class="header" id="header" role="banner">
      <div class="header-wrapper">

        <?php if ($site_name || $site_slogan): ?>
          <hgroup id="name-and-slogan" class="name-and-slogan">
            <?php if ($site_name): ?>
              <h1 id="site-name" class="sitename">
                <?php if ($logo): ?>
                  <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo" class="site-logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
                <?php endif; ?>
              </h1>
            <?php endif; ?>
          </hgroup><!-- /#name-and-slogan -->
        <?php endif; ?>

        <?php if ($secondary_menu): ?>
          <nav id="secondary-menu" role="navigation" class="secondary-menu">
            <?php print theme('links__system_secondary_menu', array(
              'links' => $secondary_menu,
              'attributes' => array(
                'class' => array('links', 'inline', 'clearfix'),
              ),
              'heading' => array(
                'text' => $secondary_menu_heading,
                'level' => 'h2',
                'class' => array('element-invisible'),
              ),
            )); ?>
          </nav>
        <?php endif; ?>

        <?php print render($page['header']); ?>

      </div>
    </header>

    <?php include 'html/components/tabs.php';?>
    <?php include 'html/components/breadcrumbs.php';?>

    <div class="component--title">
      <h2>Tiles micro</h2>
      <ul>
        <li><strong>HTML:</strong> /templates//html/components/tile-micro.php</li>
        <li><strong>SASS:</strong> /scss/base/_tile-micro.scss</li>
      </ul>
    </div>
    <?php include 'html/components/tile-micro.php';?>

    <div class="component--title">
      <h2>Card compact</h2>
      <ul>
        <li><strong>HTML:</strong> /templates//html/components/card-compact.php</li>
        <li><strong>SASS:</strong> /scss/base/_card-compact.scss</li>
      </ul>
    </div>
    <?php include 'html/components/card-compact.php';?>

    <div class="component--title">
      <h2>Atoms</h2>
      <ul>
        <li><strong>HTML:</strong> /templates//html/components/boileplate.php</li>
        <li><strong>SASS:</strong> /scss/base/_base.scss</li>
      </ul>
    </div>
    <?php include 'html/boilerplate.php';?>

    <div class="component--title">
      <h2>Menu collapsible</h2>
      <ul>
        <li><strong>HTML:</strong> /templates//html/components/menu-collapsible.php</li>
        <li><strong>SASS:</strong> /scss/components/_menu-collapsible.scss</li>
        <li><strong>JS:</strong> /js/menu-collapsible.js</li>
      </ul>
    </div>
    <?php include 'html/components/menu-collapsible.php';?>

    <div class="component--title">
      <h2>Pagination</h2>
      <ul>
        <li><strong>HTML:</strong> /templates//html/components/pagination.php</li>
        <li><strong>SASS:</strong> /scss/components/_pagination.scss</li>
      </ul>
    </div>
    <?php include 'html/components/pagination.php';?>

    <div class="component--title">
      <h2>Accordion</h2>
      <ul>
        <li><strong>HTML:</strong> /templates//html/components/accordion.php</li>
        <li><strong>SASS:</strong> /scss/components/_accordion.scss</li>
      </ul>
    </div>
    <?php include 'html/components/accordion.php';?>

    <?php print $page; ?>
    <?php print $page_bottom; ?>
  </body>
</html>
