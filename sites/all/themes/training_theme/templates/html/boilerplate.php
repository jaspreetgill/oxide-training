<?php include 'includes/head.php';?>

<div id="content" role="main">
  <div class="container container-content">
    <article class="main-content">
      <h1>Heading one</h1>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Felis est, nec convallis dui. Nulla lectus velit, ornare sit amet blandit in, mattis eget null. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Felis est, nec convallis dui. Nulla lectus velit, ornare sit amet blandit in, mattis eget null. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Felis est, nec convallis dui. Nulla lectus velit, ornare sit amet blandit in, mattis eget null.</p>
      <h2>Heading two</h2>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Felis est, nec convallis dui. Nulla lectus velit, ornare sit amet blandit in, mattis eget null. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Felis est, nec convallis dui. Nulla lectus velit, ornare sit amet blandit in, mattis eget null. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Felis est, nec convallis dui. Nulla lectus velit, ornare sit amet blandit in, mattis eget null.</p>
      <h3>Heading three</h3>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Felis est, nec convallis dui. Nulla lectus velit, ornare sit amet blandit in, mattis eget null. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Felis est, nec convallis dui. Nulla lectus velit, ornare sit amet blandit in, mattis eget null. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Felis est, nec convallis dui. Nulla lectus velit, ornare sit amet blandit in, mattis eget null.</p>
      <h4>Heading four</h4>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Felis est, nec convallis dui. Nulla lectus velit, ornare sit amet blandit in, mattis eget null. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Felis est, nec convallis dui. Nulla lectus velit, ornare sit amet blandit in, mattis eget null. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Felis est, nec convallis dui. Nulla lectus velit, ornare sit amet blandit in, mattis eget null.</p>
      <blockquote>This, on the otherhand, is a blockquote, meant for large quotes from other sources. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Felis est, nec convallis dui.</blockquote>
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Felis est, nec convallis dui. Nulla lectus velit, ornare sit amet blandit in, mattis eget null. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Felis est, nec convallis dui. Nulla lectus velit, ornare sit amet blandit in, mattis eget null. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Felis est, nec convallis dui. Nulla lectus velit, ornare sit amet blandit in, mattis eget null.</p>
      <h4>Superscript</h4>
      <p><small>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Felis est, nec convallis dui. Nulla lectus velit, ornare sit amet blandit in, mattis eget null. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Felis est, nec convallis dui. Nulla lectus velit, ornare sit amet blandit in, mattis eget null. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Felis est, nec convallis dui. Nulla lectus velit, ornare sit amet blandit in, mattis eget null.</small></p>

      <h2>Lists</h2>
      <ul>
        <li>
          This is an unordered list – ipsum dolor sit amet, consectetur adipiscing elit fringilla libero id quam semper semper.
        </li>
        <li>
          Bar is listed second – Cras fringilla libero id quam semper semper ipsum dolor sit amet, consectetur adipiscing elit fringilla libero id quam semper semper.
        </li>
      </ul>
      <ol>
        <li>Wake up in the morning</li>
        <li>Freshen up and go downstairs</li>
        <li>Have a bowl of cereal</li>
        <li>Go to the bus stop</li>
      </ol>
      <dl>
        <dt>iPod</dt>
        <dd>The iPod is a line of portable media players designed and marketed by Apple Inc.</dd>
        <dt>VCR</dt>
        <dd>The VCR is an electro-mechanical device that records analog audio and analog video from broadcast television on a removable tape.</dd>
        <dt>Walkman</dt>
        <dd>Walkman is a Sony brand tradename originally used for portable audio cassette players.</dd>
      </dl>
      <h2>Tables</h2>
      <table>
        <caption>Table with caption, colgroup, thead, tfoot, and tbody</caption>
        <colgroup>
          <col />
          <col />
          <col />
        </colgroup>
        <thead>
          <tr>
            <th>header 1</th>
            <th>header 2</th>
            <th>header 3</th>
          </tr>
        </thead>
        <tfoot>
          <tr>
            <td>footer 1</td>
            <td>footer 2</td>
            <td>footer 3</td>
          </tr>
        </tfoot>
        <tbody>
          <tr>
            <td>body 1</td>
            <td>body 2</td>
            <td>body 3</td>
          </tr>
          <tr>
            <td>body 1</td>
            <td>body 2</td>
            <td>body 3</td>
          </tr>
          <tr>
            <td>body 1</td>
            <td>body 2</td>
            <td>body 3</td>
          </tr>
          <tr>
            <td>body 1</td>
            <td>body 2</td>
            <td>body 3</td>
          </tr>
        </tbody>
      </table>
      <h2>Notices</h2>
      <div class="success">
        <p><strong>Success</strong> – Message with a <a href="#">link</a>.</p>
      </div>
      <div class="info">
        <p><strong>Info</strong> – Message with a <a href="#">link</a>.</p>
      </div>
      <div class="error">
        <p><strong>Error</strong> – Message with a <a href="#">link</a>.</p>
        <ul>
          <li>Item one: message regarding item one</li>
          <li>Item two: message regarding item two</li>
          <li>Item three: message regarding item three</li>
        </ul>
      </div>
      <form method="post">
        <h2>Form elements</h2>
        <fieldset>
          <p>Through out the fields, you will also see <code>div.help</code> and <code>span.help</code> elements. These are used for help text relevant to fields.</p>
          <div class="text">
            <label for="sample_form-text">Text <span class="required">*</span></label>
            <input id="sample_form-text" type="text" />
            <div class="help">only numbers and letters</div>
          </div>
          <div class="password">
            <label for="sample_form-password">Password <span class="required">*</span></label>
            <input id="sample_form-password" type="password" />
          </div>
        </fieldset>
        <fieldset>
          <div class="radios group">
            <label>Choose one</label>
            <label class="radio"><input name="sample" type="radio" /> radio 1</label>
            <label class="radio"><input name="sample" type="radio" /> radio 2</label>
          </div>
          <div class="checkboxes group">
            <label>Check all that apply</label>
            <label class="checkbox"><input type="checkbox" /> checkbox 1</label>
            <label class="checkbox"><input type="checkbox" /> checkbox 2</label>
          </div>
        </fieldset>
        <fieldset>
          <div class="select">
            <label for="sample_form-select">Select one</label>
            <select id="sample_form-select">
              <optgroup label="group">
                <option value="option 1">option 1</option>
                <option value="option 2" selected="selected">option 2</option>
              </optgroup>
              <optgroup label="group 1">
                <option value="option 3">option 3</option>
                <option value="option 4">option 4</option>
                <option value="option 5">option 5</option>
                <option value="option 6">option 6</option>
              </optgroup>
            </select>
          </div>
          <div class="select multiple">
            <label for="sample_form-select_multiple">Select multiple</label>
            <select id="sample_form-select_multiple" multiple="multiple">
              <optgroup label="group">
                <option value="option">Option</option>
                <option value="option 1">option 1</option>
                <option value="option 2" selected="selected">option 2</option>
              </optgroup>
              <optgroup label="group 1">
                <option value="option 3">option 3</option>
                <option value="option 4">option 4</option>
                <option value="option 5">option 5</option>
                <option value="option 6">option 6</option>
                <option value="option 7">option 7</option>
                <option value="option 8">option 8</option>
              </optgroup>
          </select>
        </div>
        </fieldset>
        <fieldset>
          <div class="textarea">
            <label for="sample_form-textarea">Textarea</label>
            <textarea id="sample_form-textarea">Textarea</textarea>
          </div>
        </fieldset>
        <fieldset>
          <h2>Buttons</h2>
          <div class="buttons">
            <p><a class="button" href="#">Link button</a></p>
            <p><a class="button button-secondary" href="#">Link button-secondary</a></p>
            <p style="background:#aaa;padding:1rem;"><a class="button button-reversed" href="#">Link button-reversed</a></p>
            <p><a class="button button-primary" href="#">Link button-primary</a></p>
            <p><a class="button button-small" href="#">Link button-small</a></p>
            <p><a class="button button-small-secondary" href="#">Link button-small-secondary</a></p>
            <p style="background:#aaa;padding:1rem;"><a class="button button-small-reversed" href="#">Link button-small-reversed</a></p>
            <p><input type="submit" value="Submit" /></p>
            <p><input type="reset" value="Reset" /></p>
            <p><input type="submit" value="Disabled" disabled="disabled" /></p>
            <p><button>Button element</button></p>
          </div>
        </fieldset>
      </form>
    </article>
  </div>
</div>

<!--#include virtual="/html/includes/footer.html" -->
<?php include 'includes/footer.php';?>
