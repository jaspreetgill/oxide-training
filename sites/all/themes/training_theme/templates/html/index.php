<?php include 'head.php';?>

<header>
  <?php include 'components/header-logo.php';?>
</header>

<?php include 'components/tabs.php';?>
<?php include 'components/breadcrumbs.php';?>
<?php include 'components/tile-micro.php';?>
<?php include 'components/card-compact.php';?>
<?php include 'boilerplate.php';?>
<?php include 'components/pagination.php';?>

<?php include 'footer.php'; ?>