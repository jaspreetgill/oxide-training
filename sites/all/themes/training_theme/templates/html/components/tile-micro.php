<!-- Include list of links to each components -->
<div class="view view-plan-your-visit view-id-plan_your_visit view-display-id-block view-dom-id-00d474d4d5a288959f5930c003d9a090">
  <div class="view-content">
    <div class="item-list">
      <ul>
        <li class="views-row views-row-1 views-row-odd views-row-first">
          <div class="ds-1col node node-level3 view-mode-mini_tile view-mode-mini_tile clearfix ">
            <a href="#" class="group-link field-group-link hasImg">
              <div class="field field-name-field-homepage-icon field-type-image field-label-hidden">
                <div class="field-items">
                  <div class="field-item even"><img src="/sites/all/themes/custom/oxide_patterns/images/icon-close.svg" width="100" height="100" aria-hidden="true"></div>
                </div>
              </div>
              <div class="field field-name-title field-type-ds field-label-hidden">
                <div class="field-items">
                  <div class="field-item even">
                    <h3>Action tile title</h3>
                  </div>
                </div>
              </div>
            </a>
          </div>
        </li>
        <li class="views-row views-row-2 views-row-even">
          <div class="ds-1col node node-level3 view-mode-mini_tile view-mode-mini_tile clearfix ">
            <a href="#" class="group-link field-group-link hasImg">
              <div class="field field-name-field-homepage-icon field-type-image field-label-hidden">
                <div class="field-items">
                  <div class="field-item even"><img src="/sites/all/themes/custom/oxide_patterns/images/icon-heart.svg" width="100" height="100" aria-hidden="true"></div>
                </div>
              </div>
              <div class="field field-name-title field-type-ds field-label-hidden">
                <div class="field-items">
                  <div class="field-item even">
                    <h3>Action tile title</h3>
                  </div>
                </div>
              </div>
            </a>
          </div>
        </li>
        <li class="views-row views-row-3 views-row-odd">
          <div class="ds-1col node node-level3 view-mode-mini_tile view-mode-mini_tile clearfix ">
            <a href="#" class="group-link field-group-link hasImg">
              <div class="field field-name-field-homepage-icon field-type-image field-label-hidden">
                <div class="field-items">
                  <div class="field-item even"><img src="/sites/all/themes/custom/oxide_patterns/images/icon-home.svg" width="100" height="100" aria-hidden="true"></div>
                </div>
              </div>
              <div class="field field-name-title field-type-ds field-label-hidden">
                <div class="field-items">
                  <div class="field-item even">
                    <h3>Action tile title</h3>
                  </div>
                </div>
              </div>
            </a>
          </div>
        </li>
        <li class="views-row views-row-4 views-row-even">
          <div class="ds-1col node node-level3 view-mode-mini_tile view-mode-mini_tile clearfix ">
            <a href="#" class="group-link field-group-link hasImg">
              <div class="field field-name-field-homepage-icon field-type-image field-label-hidden">
                <div class="field-items">
                  <div class="field-item even"><img src="/sites/all/themes/custom/oxide_patterns/images/icon-support.svg" width="100" height="100" aria-hidden="true"></div>
                </div>
              </div>
              <div class="field field-name-title field-type-ds field-label-hidden">
                <div class="field-items">
                  <div class="field-item even">
                    <h3>Action tile title</h3>
                  </div>
                </div>
              </div>
            </a>
          </div>
        </li>
        <li class="views-row views-row-5 views-row-odd">
          <div class="ds-1col node node-level3 view-mode-mini_tile view-mode-mini_tile clearfix ">
            <a href="#" class="group-link field-group-link hasImg">
              <div class="field field-name-field-homepage-icon field-type-image field-label-hidden">
                <div class="field-items">
                  <div class="field-item even"><img src="/sites/all/themes/custom/oxide_patterns/images/icon-phone.svg" width="100" height="100" aria-hidden="true"></div>
                </div>
              </div>
              <div class="field field-name-title field-type-ds field-label-hidden">
                <div class="field-items">
                  <div class="field-item even">
                    <h3>Action tile title</h3>
                  </div>
                </div>
              </div>
            </a>
          </div>
        </li>
        <li class="views-row views-row-6 views-row-even views-row-last">
          <div class="ds-1col node node-level3 view-mode-mini_tile view-mode-mini_tile clearfix ">
            <a href="#" class="group-link field-group-link hasImg">
              <div class="field field-name-field-homepage-icon field-type-image field-label-hidden">
                <div class="field-items">
                  <div class="field-item even"><img src="/sites/all/themes/custom/oxide_patterns/images/icon-star.svg" width="100" height="100" aria-hidden="true"></div>
                </div>
              </div>
              <div class="field field-name-title field-type-ds field-label-hidden">
                <div class="field-items">
                  <div class="field-item even">
                    <h3>Action tile title</h3>
                  </div>
                </div>
              </div>
            </a>
          </div>
        </li>
      </ul>
    </div>
  </div>
</div>