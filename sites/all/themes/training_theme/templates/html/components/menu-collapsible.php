<aside class="sidebars">
  <section class="region region-sidebar-first column sidebar">
    <div class="block block-menu-block nav first last odd" id="block-menu-block-1" role="navigation">
      <h2 class="block__title block-title"><a class="active-trail" href="#">Using the Library</a></h2>
      <div class="menu-block-wrapper menu-block-1 menu-name-navigation parent-mlid-0 menu-level-2">
        <ul class="menu nav">
          <li class="menu__item is-active is-active-trail is-expanded first expanded active-trail active menu-mlid-7036">
            <a class="menu__link is-active-trail active-trail active" href="/getting-started">Getting started</a>
            <ul class="menu nav">
              <li class="menu__item is-leaf first leaf menu-mlid-3976">
                <a class="menu__link" href="#">Get a Library card</a>
              </li>
              <li class="menu__item is-leaf leaf menu-mlid-7046">
                <a class="menu__link" href="#">Ask a Librarian</a>
              </li>
              <li class="menu__item is-leaf leaf menu-mlid-6744">
                <a class="menu__link" href="#">Frequently asked questions</a>
              </li>
              <li class="menu__item is-expanded last expanded menu-mlid-7045">
                <a class="menu__link" href="#">Getting started videos</a>
                <ul class="menu nav">
                  <li class="menu__item is-leaf first leaf menu-mlid-8778">
                    <a class="menu__link" href="#">Can I get copies of items from the Library?</a>
                  </li>
                  <li class="menu__item is-leaf leaf menu-mlid-8777">
                    <a class="menu__link" href="#">Can I read newspapers online?</a>
                  </li>
                  <li class="menu__item is-leaf leaf menu-mlid-8776">
                    <a class="menu__link" href="#">Experimenting with the catalogue</a>
                  </li>
                  <li class="menu__item is-leaf leaf menu-mlid-8780">
                    <a class="menu__link" href="#">How do I research my family history?</a>
                  </li>
                  <li class="menu__item is-expanded expanded menu-mlid-8769">
                    <a class="menu__link" href="#">Transcripts - Getting Started Videos</a>
                    <ul class="menu nav">
                      <li class="menu__item is-leaf first leaf menu-mlid-8770">
                        <a class="menu__link" href="#">Transcript - Can I borrow items from the Library?</a>
                      </li>
                      <li class="menu__item is-leaf leaf menu-mlid-8771">
                        <a class="menu__link" href="#">Transcript - Experimenting with the catalogue</a>
                      </li>
                      <li class="menu__item is-leaf leaf menu-mlid-8772">
                        <a class="menu__link" href="#">Transcript - Can I read newspapers online?</a>
                      </li>
                      <li class="menu__item is-leaf leaf menu-mlid-8773">
                        <a class="menu__link" href="#">Transcript - Can I get copies of items from the Library?</a>
                      </li>
                      <li class="menu__item is-leaf leaf menu-mlid-8774">
                        <a class="menu__link" href="#">Transcript - What can I get online?</a>
                      </li>
                      <li class="menu__item is-leaf last leaf menu-mlid-8775">
                        <a class="menu__link" href="#">Transcript - How do I research my family history?</a>
                      </li>
                    </ul>
                  </li>
                  <li class="menu__item is-leaf last leaf menu-mlid-8779">
                    <a class="menu__link" href="#">What can I get online?</a>
                  </li>
                </ul>
              </li>
            </ul>
          </li>
          <li class="menu__item is-expanded expanded menu-mlid-7037">
            <a class="menu__link" href="#">Learning</a>
            <ul class="menu nav">
              <li class="menu__item is-expanded first expanded menu-mlid-5386">
                <a class="menu__link" href="#">Learning program</a>
                <ul class="menu nav">
                  <li class="menu__item is-leaf first leaf menu-mlid-8881">
                    <a class="menu__link" href="#">Joining a Webinar</a>
                  </li>
                  <li class="menu__item is-leaf last leaf menu-mlid-8880">
                    <a class="menu__link" href="#">Past Webinar Recordings</a>
                  </li>
                </ul>
              </li>
              <li class="menu__item is-expanded last expanded menu-mlid-7047">
                <a class="menu__link" href="#">School and teacher program</a>
                <ul class="menu nav">
                  <li class="menu__item is-leaf first leaf menu-mlid-3579">
                    <a class="menu__link" href="#">School programs and guided tours</a>
                  </li>
                  <li class="menu__item is-leaf leaf menu-mlid-3578">
                    <a class="menu__link" href="#">Planning your school visit</a>
                  </li>
                  <li class="menu__item is-leaf leaf menu-mlid-6091">
                    <a class="menu__link" href="#">Make a group booking</a>
                  </li>
                  <li class="menu__item is-leaf last leaf menu-mlid-7055">
                    <a class="menu__link" href="#">Contact us</a>
                  </li>
                </ul>
              </li>
            </ul>
          </li>
          <li class="menu__item is-expanded expanded menu-mlid-824">
            <a class="menu__link" href="#" title="Reading rooms">Reading rooms</a>
            <ul class="menu nav">
              <li class="menu__item is-leaf first leaf menu-mlid-7043">
                <a class="menu__link" href="#">Using the reading rooms</a>
              </li>
              <li class="menu__item is-expanded expanded menu-mlid-826">
                <a class="menu__link" href="#" title="Main Reading Room">Main Reading Room</a>
                <ul class="menu nav">
                  <li class="menu__item is-leaf first last leaf menu-mlid-6340">
                    <a class="menu__link" href="#">Newspapers and Family History</a>
                  </li>
                </ul>
              </li>
              <li class="menu__item is-expanded expanded menu-mlid-6230">
                <a class="menu__link" href="#">Special Collections Reading Room</a>
                <ul class="menu nav">
                  <li class="menu__item is-expanded first last expanded menu-mlid-6240">
                    <a class="menu__link" href="#">Using our Special Collections</a>
                    <ul class="menu nav">
                      <li class="menu__item is-leaf first last leaf menu-mlid-3815">
                        <a class="menu__link" href="#">Rare books</a>
                      </li>
                    </ul>
                  </li>
                </ul>
              </li>
              <li class="menu__item is-expanded expanded menu-mlid-830">
                <a class="menu__link" href="#" title="Petherick Reading Room">Petherick Reading Room</a>
                <ul class="menu nav">
                  <li class="menu__item is-leaf first leaf menu-mlid-7158">
                    <a class="menu__link" href="#">Petherick Guidelines</a>
                  </li>
                  <li class="menu__item is-leaf last leaf menu-mlid-1265">
                    <a class="menu__link" href="#" title="Petherick Registration">Petherick Registration</a>
                  </li>
                </ul>
              </li>
              <li class="menu__item is-leaf leaf menu-mlid-825">
                <a class="menu__link" href="#" title="Asian Collections Reading Room">Asian Collections Reading Room</a>
              </li>
              <li class="menu__item is-expanded expanded menu-mlid-2362">
                <a class="menu__link" href="#" title="Collection delivery service">Collection delivery service</a>
                <ul class="menu nav">
                  <li class="menu__item is-leaf first last leaf menu-mlid-5974">
                    <a class="menu__link" href="#">West Stack Story: collection delivery video</a>
                  </li>
                </ul>
              </li>
              <li class="menu__item is-leaf leaf menu-mlid-793">
                <a class="menu__link" href="#" title="Collection delivery times">Collection delivery times</a>
              </li>
              <li class="menu__item is-leaf last leaf menu-mlid-1054">
                <a class="menu__link" href="#" title="Acceptable use of information and communication technology">Acceptable use of ICT</a>
              </li>
            </ul>
          </li>
          <li class="menu__item is-expanded expanded menu-mlid-7038">
            <a class="menu__link" href="#">Copies and interlibrary loans</a>
            <ul class="menu nav">
              <li class="menu__item is-expanded first expanded menu-mlid-3362">
                <a class="menu__link" href="#">Copies and interlibrary loans for individuals</a>
                <ul class="menu nav">
                  <li class="menu__item is-leaf first leaf menu-mlid-3367">
                    <a class="menu__link" href="#">Copies Direct delivery options</a>
                  </li>
                  <li class="menu__item is-leaf leaf menu-mlid-3364">
                    <a class="menu__link" href="#">Copies Direct delivery times</a>
                  </li>
                  <li class="menu__item is-leaf leaf menu-mlid-3361">
                    <a class="menu__link" href="#">Copies Direct payment methods</a>
                  </li>
                  <li class="menu__item is-leaf leaf menu-mlid-3363">
                    <a class="menu__link" href="#">Copies Direct prices</a>
                  </li>
                  <li class="menu__item is-leaf last leaf menu-mlid-3366">
                    <a class="menu__link" href="#">How to use the Copies Direct service</a>
                  </li>
                </ul>
              </li>
              <li class="menu__item is-expanded expanded menu-mlid-1323">
                <a class="menu__link" href="#" title="Document supply services">Copies and interlibrary loans for libraries</a>
                <ul class="menu nav">
                  <li class="menu__item is-leaf first leaf menu-mlid-3305">
                    <a class="menu__link" href="#">Borrowing Conditions</a>
                  </li>
                  <li class="menu__item is-leaf leaf menu-mlid-3303">
                    <a class="menu__link" href="#">Charges, payment methods and service levels</a>
                  </li>
                  <li class="menu__item is-leaf leaf menu-mlid-3308">
                    <a class="menu__link" href="#">Contact Document Supply Service</a>
                  </li>
                  <li class="menu__item is-leaf leaf menu-mlid-3306">
                    <a class="menu__link" href="#" title="Copyright and document supply for libraries">Copyright and document supply for libraries</a>
                  </li>
                  <li class="menu__item is-leaf leaf menu-mlid-3365">
                    <a class="menu__link" href="#">Loansome Doc</a>
                  </li>
                  <li class="menu__item is-leaf last leaf menu-mlid-3307">
                    <a class="menu__link" href="#">Useful links</a>
                  </li>
                </ul>
              </li>
              <li class="menu__item is-expanded expanded menu-mlid-1296">
                <a class="menu__link" href="/supply-1" title="Supply 1">Supply 1 for libraries</a>
                <ul class="menu nav">
                  <li class="menu__item is-leaf first last leaf menu-mlid-1297">
                    <a class="menu__link" href="#" title="How to use Supply 1">How to use Supply 1</a>
                  </li>
                </ul>
              </li>
              <li class="menu__item is-leaf last leaf menu-mlid-7048">
                <a class="menu__link" href="#">Copyright in library collections</a>
              </li>
            </ul>
          </li>
          <li class="menu__item is-expanded expanded menu-mlid-7039">
            <a class="menu__link" href="#">Research tools and resources</a>
            <ul class="menu nav">
              <li class="menu__item is-leaf first leaf menu-mlid-3458">
                <a class="menu__link" href="#">Private researchers and valuers</a>
              </li>
              <li class="menu__item is-leaf leaf menu-mlid-6552">
                <a class="menu__link" href="#">Published guides and bibliographies</a>
              </li>
              <li class="menu__item is-leaf leaf menu-mlid-7050">
                <a class="menu__link" href="#">Research guides</a>
              </li>
              <li class="menu__item is-expanded last expanded menu-mlid-8782">
                <a class="menu__link" href="#">Australian Joint Copying Project</a>
                <ul class="menu nav">
                  <li class="menu__item is-leaf first leaf menu-mlid-9766">
                    <a class="menu__link" href="#">Overview of AJCP content</a>
                  </li>
                  <li class="menu__item is-leaf leaf menu-mlid-9765">
                    <a class="menu__link" href="#">Using AJCP</a>
                  </li>
                  <li class="menu__item is-leaf leaf menu-mlid-9768">
                    <a class="menu__link" href="#">AJCP PRO Series</a>
                  </li>
                  <li class="menu__item is-leaf leaf menu-mlid-9769">
                    <a class="menu__link" href="#">AJCP M Series</a>
                  </li>
                  <li class="menu__item is-leaf leaf menu-mlid-9770">
                    <a class="menu__link" href="#">History of AJCP</a>
                  </li>
                  <li class="menu__item is-leaf leaf menu-mlid-9767">
                    <a class="menu__link" href="#">Project News</a>
                  </li>
                  <li class="menu__item is-leaf last leaf menu-mlid-9771">
                    <a class="menu__link" href="#">Citation of AJCP Content</a>
                  </li>
                </ul>
              </li>
            </ul>
          </li>
          <li class="menu__item is-leaf leaf menu-mlid-7041">
            <a class="menu__link" href="#">Services for libraries</a>
          </li>
          <li class="menu__item is-expanded last expanded menu-mlid-7040">
            <a class="menu__link" href="#">Services for publishers</a>
            <ul class="menu nav">
              <li class="menu__item is-expanded first expanded menu-mlid-1301">
                <a class="menu__link" href="#" title="Legal Deposit">Legal deposit</a>
                <ul class="menu nav">
                  <li class="menu__item is-leaf first leaf menu-mlid-6732">
                    <a class="menu__link" href="#">What is legal deposit?</a>
                  </li>
                  <li class="menu__item is-leaf leaf menu-mlid-6733">
                    <a class="menu__link" href="#">How to deposit</a>
                  </li>
                  <li class="menu__item is-leaf leaf menu-mlid-6766">
                    <a class="menu__link" href="#">Access to electronic publications</a>
                  </li>
                  <li class="menu__item is-leaf leaf menu-mlid-7162">
                    <a class="menu__link" href="#">Creative Commons and electronic publications</a>
                  </li>
                  <li class="menu__item is-leaf leaf menu-mlid-3298">
                    <a class="menu__link" href="#" title="Legal Deposit FAQs">FAQs</a>
                  </li>
                  <li class="menu__item is-leaf leaf menu-mlid-3297">
                    <a class="menu__link" href="#">Legal deposit Australia wide</a>
                  </li>
                  <li class="menu__item is-leaf last leaf menu-mlid-6734">
                    <a class="menu__link" href="#">Contact us</a>
                  </li>
                </ul>
              </li>
              <li class="menu__item is-expanded expanded menu-mlid-8747">
                <a class="menu__link" href="#">Prepublication Data Service</a>
                <ul class="menu nav">
                  <li class="menu__item is-leaf first leaf menu-mlid-8728">
                    <a class="menu__link" href="#">Changes to the CiP service</a>
                  </li>
                  <li class="menu__item is-leaf leaf menu-mlid-8739">
                    <a class="menu__link" href="#">Cataloguing statement</a>
                  </li>
                  <li class="menu__item is-leaf leaf menu-mlid-8748">
                    <a class="menu__link" href="#">Eligibility</a>
                  </li>
                  <li class="menu__item is-leaf leaf menu-mlid-8749">
                    <a class="menu__link" href="#">User guide</a>
                  </li>
                  <li class="menu__item is-leaf last leaf menu-mlid-8750">
                    <a class="menu__link" href="#">FAQs</a>
                  </li>
                </ul>
              </li>
              <li class="menu__item is-expanded last expanded menu-mlid-7042">
                <a class="menu__link" href="#">Apply for an ISBN, ISSN or ISMN</a>
                <ul class="menu nav">
                  <li class="menu__item is-leaf first leaf menu-mlid-1292">
                    <a class="menu__link" href="#" title="The Australian ISBN Agency">The Australian ISBN Agency</a>
                  </li>
                  <li class="menu__item is-expanded expanded menu-mlid-1293">
                    <a class="menu__link" href="#" title="The Australian ISSN Agency">The Australian ISSN Agency</a>
                    <ul class="menu nav">
                      <li class="menu__item is-leaf first leaf menu-mlid-1294">
                        <a class="menu__link" href="#" title="About ISSNs - International Standard Serial Numbers">About ISSNs</a>
                      </li>
                      <li class="menu__item is-leaf last leaf menu-mlid-3946">
                        <a class="menu__link" href="#">Apply for an Australian ISSN</a>
                      </li>
                    </ul>
                  </li>
                  <li class="menu__item is-expanded last expanded menu-mlid-1790">
                    <a class="menu__link" href="#" title="The Australian ISMN Agency">The Australian ISMN Agency</a>
                    <ul class="menu nav">
                      <li class="menu__item is-leaf first leaf menu-mlid-3765">
                        <a class="menu__link" href="#" title="About ISMNs">About ISMNs</a>
                      </li>
                      <li class="menu__item is-leaf leaf menu-mlid-3767">
                        <a class="menu__link" href="#">ISMN FAQs</a>
                      </li>
                      <li class="menu__item is-leaf last leaf menu-mlid-3766">
                        <a class="menu__link" href="#">ISMN Related services</a>
                      </li>
                    </ul>
                  </li>
                </ul>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  </section>
</aside>