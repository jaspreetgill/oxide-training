<div class="search-wrapper show" aria-expanded="false" aria-live="assertive"><a class="search-toggle" href="#" tabindex="0" title="Search this website, Catalogue, and eResources" aria-expanded="true">Toggle search</a>
  <div class="header__region region region-header">
    <div id="block-views-exp-search-view-page" class="block block-views first last odd">
      <form action="/search" method="get" id="views-exposed-form-search-view-page" accept-charset="UTF-8" _lpchecked="1">
        <div>
          <div class="views-exposed-form">
            <div class="views-exposed-widgets clearfix">
              <div id="edit-search-api-views-fulltext-wrapper" class="views-exposed-widget views-widget-filter-search_api_views_fulltext">
                <div class="views-widget">
                  <div class="form-item form-type-textfield form-item-search-api-views-fulltext">
                    <label for="edit-search-api-views-fulltext">Search this website </label>
                    <input onfocus="if (this.value == 'Search this site') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Search this site';}" title="search website" type="text" id="edit-search-api-views-fulltext" name="search_api_views_fulltext"
                      value="Search this site" size="30" maxlength="128" class="form-text" tabindex="0">
                  </div>
                </div>
              </div>
              <div class="views-exposed-widget views-submit-button">
                <span class="glyphicon glyphicon-search form-control-feedback"></span><input type="submit" id="edit-submit-search-view" name="" value="Search" class="form-submit" tabindex="0"> </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
