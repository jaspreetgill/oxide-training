<div class="view view-collections-block view-id-collections_block view-display-id-block_1 view-dom-id-0d8a07f760977a8cf2157c2c86ad1d8d">
  <div class="view-header">
    <a href="/collections" class="button more-link">Browse all collections</a>
  </div>
  <div class="view-content">
    <div class="item-list">
      <ul>
        <li class="views-row views-row-1 views-row-odd views-row-first">
          <div class="ds-1col node node-level3 view-mode-tile view-mode-tile clearfix ">
            <a href="/what-we-collect/archived-websites" class="group-link field-group-link hasImg">
              <div class="field field-name-field-level3-image field-type-image field-label-hidden">
                <div class="field-items">
                  <div class="field-item even"><img src="https://www.nla.gov.au/sites/default/files/styles/tile_image/public/olympic.jpg?itok=3yZXyVkY" width="500" height="500" aria-hidden="true">
                  </div>
                </div>
              </div>
              <div class="field field-name-title field-type-ds field-label-hidden">
                <div class="field-items">
                  <div class="field-item even">
                    <h3>Archived websites</h3>
                  </div>
                </div>
              </div>
            </a>
          </div>
        </li>
        <li class="views-row views-row-2 views-row-even">
          <div class="ds-1col node node-level3 view-mode-tile view-mode-tile clearfix ">
            <a href="/what-we-collect/asian" class="group-link field-group-link hasImg">
              <div class="field field-name-field-level3-image field-type-image field-label-hidden">
                <div class="field-items">
                  <div class="field-item even"><img src="https://www.nla.gov.au/sites/default/files/styles/tile_image/public/nla.obj-152406847-1_0.jpg?itok=Pz4dZXsh" width="500" height="500" aria-hidden="true">
                  </div>
                </div>
              </div>
              <div class="field field-name-title field-type-ds field-label-hidden">
                <div class="field-items">
                  <div class="field-item even">
                    <h3>Asian</h3>
                  </div>
                </div>
              </div>
            </a>
          </div>
        </li>
        <li class="views-row views-row-3 views-row-odd">
          <div class="ds-1col node node-level3 view-mode-tile view-mode-tile clearfix ">


            <a href="/what-we-collect/dance" class="group-link field-group-link hasImg">
              <div class="field field-name-field-level3-image field-type-image field-label-hidden">
                <div class="field-items">
                  <div class="field-item even"><img src="https://www.nla.gov.au/sites/default/files/styles/tile_image/public/nla.obj-137291493-1_2.jpg?itok=tUZLYHPO" width="500" height="500" aria-hidden="true">
                  </div>
                </div>
              </div>
              <div class="field field-name-title field-type-ds field-label-hidden">
                <div class="field-items">
                  <div class="field-item even">
                    <h3>Dance</h3>
                  </div>
                </div>
              </div>
            </a>
          </div>
        </li>
        <li class="views-row views-row-4 views-row-even">
          <div class="ds-1col node node-level3 view-mode-tile view-mode-tile clearfix ">


            <a href="/what-we-collect/ephemera" class="group-link field-group-link hasImg">
              <div class="field field-name-field-level3-image field-type-image field-label-hidden">
                <div class="field-items">
                  <div class="field-item even"><img src="https://www.nla.gov.au/sites/default/files/styles/tile_image/public/ephemera_hero_shot.jpg?itok=lDuExJUY" width="500" height="500" alt="Detail from a P &amp; O Line shipping menu from the ephemera collection" title="Detail from a P &amp; O Line shipping menu from the ephemera collection" aria-hidden="true">
                  </div>
                </div>
              </div>
              <div class="field field-name-title field-type-ds field-label-hidden">
                <div class="field-items">
                  <div class="field-item even">
                    <h3>Printed ephemera</h3>
                  </div>
                </div>
              </div>
            </a>
          </div>
        </li>
        <li class="views-row views-row-5 views-row-odd">
          <div class="ds-1col node node-level3 view-mode-tile view-mode-tile clearfix ">


            <a href="/what-we-collect/indigenous" class="group-link field-group-link hasImg">
              <div class="field field-name-field-level3-image field-type-image field-label-hidden">
                <div class="field-items">
                  <div class="field-item even"><img src="https://www.nla.gov.au/sites/default/files/styles/tile_image/public/nla.obj-145680129-m.jpg?itok=4VuOixBq" width="500" height="500" aria-hidden="true">
                  </div>
                </div>
              </div>
              <div class="field field-name-title field-type-ds field-label-hidden">
                <div class="field-items">
                  <div class="field-item even">
                    <h3>Indigenous</h3>
                  </div>
                </div>
              </div>
            </a>
          </div>
        </li>
        <li class="views-row views-row-6 views-row-even">
          <div class="ds-1col node node-level3 view-mode-tile view-mode-tile clearfix ">


            <a href="/what-we-collect/manuscripts" class="group-link field-group-link hasImg">
              <div class="field field-name-field-level3-image field-type-image field-label-hidden">
                <div class="field-items">
                  <div class="field-item even"><img src="https://www.nla.gov.au/sites/default/files/styles/tile_image/public/ms-hero-ms-8015-2-s1-e.jpg?itok=0dHN35FM" width="500" height="500" alt="Salute to Five bells: John Olsen's Opera House journal, 1971-1973, MS 8015-2"
                    title="ohn Olsen's Opera House journal, 1971-1973, MS 8015-2" aria-hidden="true">
                  </div>
                </div>
              </div>
              <div class="field field-name-title field-type-ds field-label-hidden">
                <div class="field-items">
                  <div class="field-item even">
                    <h3>Manuscripts</h3>
                  </div>
                </div>
              </div>
            </a>
          </div>
        </li>
        <li class="views-row views-row-7 views-row-odd">
          <div class="ds-1col node node-level3 view-mode-tile view-mode-tile clearfix ">


            <a href="/what-we-collect/maps" class="group-link field-group-link hasImg">
              <div class="field field-name-field-level3-image field-type-image field-label-hidden">
                <div class="field-items">
                  <div class="field-item even"><img src="https://www.nla.gov.au/sites/default/files/styles/tile_image/public/nla.obj-230054338-1.jpg?itok=FONGXgjI" width="500" height="500" aria-hidden="true"></div>
                </div>
              </div>
              <div class="field field-name-title field-type-ds field-label-hidden">
                <div class="field-items">
                  <div class="field-item even">
                    <h3>Maps</h3>
                  </div>
                </div>
              </div>
            </a>
          </div>
        </li>
        <li class="views-row views-row-8 views-row-even">
          <div class="ds-1col node node-level3 view-mode-tile view-mode-tile clearfix ">


            <a href="/what-we-collect/music" class="group-link field-group-link hasImg">
              <div class="field field-name-field-level3-image field-type-image field-label-hidden">
                <div class="field-items">
                  <div class="field-item even"><img src="https://www.nla.gov.au/sites/default/files/styles/tile_image/public/music-hero-nla.mus-vn000003118713-s001-m.jpg?itok=V3F7caUd" width="500" height="500" aria-hidden="true"></div>
                </div>
              </div>
              <div class="field field-name-title field-type-ds field-label-hidden">
                <div class="field-items">
                  <div class="field-item even">
                    <h3>Music</h3>
                  </div>
                </div>
              </div>
            </a>
          </div>
        </li>
        <li class="views-row views-row-9 views-row-odd">
          <div class="ds-1col node node-level3 view-mode-tile view-mode-tile clearfix">


            <a href="/what-we-collect/newspapers" class="group-link field-group-link hasImg">
              <div class="field field-name-field-level3-image field-type-image field-label-hidden">
                <div class="field-items">
                  <div class="field-item even"><img src="https://www.nla.gov.au/sites/default/files/styles/tile_image/public/nla.obj-147873263-m_0.jpg?itok=u6jMmDBX" width="500" height="500" aria-hidden="true">
                  </div>
                </div>
              </div>
              <div class="field field-name-title field-type-ds field-label-hidden">
                <div class="field-items">
                  <div class="field-item even">
                    <h3>Newspapers</h3>
                  </div>
                </div>
              </div>
            </a>
          </div>
        </li>
        <li class="views-row views-row-10 views-row-even">
          <div class="ds-1col node node-level3 view-mode-tile view-mode-tile clearfix ">
            <a href="/what-we-collect/oral-history-and-folklore" class="group-link field-group-link hasImg">
              <div class="field field-name-field-level3-image field-type-image field-label-hidden">
                <div class="field-items">
                  <div class="field-item even"><img src="https://www.nla.gov.au/sites/default/files/styles/tile_image/public/smokey_0.jpg?itok=RbzR3I7j" width="500" height="500" aria-hidden="true"></div>
                </div>
              </div>
              <div class="field field-name-title field-type-ds field-label-hidden">
                <div class="field-items">
                  <div class="field-item even">
                    <h3>Oral history and folklore</h3>
                  </div>
                </div>
              </div>
            </a>
          </div>
        </li>
        <li class="views-row views-row-11 views-row-odd">
          <div class="ds-1col node node-level3 view-mode-tile view-mode-tile clearfix ">
            <a href="/what-we-collect/pacific-collections" class="group-link field-group-link hasImg">
              <div class="field field-name-field-level3-image field-type-image field-label-hidden">
                <div class="field-items">
                  <div class="field-item even"><img src="https://www.nla.gov.au/sites/default/files/styles/tile_image/public/nla.obj-148449394-1.jpg?itok=0FC69N3d" width="500" height="500" aria-hidden="true">
                  </div>
                </div>
              </div>
              <div class="field field-name-title field-type-ds field-label-hidden">
                <div class="field-items">
                  <div class="field-item even">
                    <h3>Pacific</h3>
                  </div>
                </div>
              </div>
            </a>
          </div>
        </li>
        <li class="views-row views-row-12 views-row-even views-row-last">
          <div class="ds-1col node node-level3 view-mode-tile view-mode-tile clearfix ">
            <a href="/what-we-collect/pictures" class="group-link field-group-link hasImg">
              <div class="field field-name-field-level3-image field-type-image field-label-hidden">
                <div class="field-items">
                  <div class="field-item even"><img src="https://www.nla.gov.au/sites/default/files/styles/tile_image/public/nla.obj-150068793-1_copy.jpg?itok=GqH-L_Ju" width="500" height="500" aria-hidden="true">
                  </div>
                </div>
              </div>
              <div class="field field-name-title field-type-ds field-label-hidden">
                <div class="field-items">
                  <div class="field-item even">
                    <h3>Pictures</h3>
                  </div>
                </div>
              </div>
            </a>
          </div>
        </li>
      </ul>
    </div>
  </div>

</div>
