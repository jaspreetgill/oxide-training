<div id="block-bean-collections-search" class="block block-bean first odd">
  <div class="entity entity-bean bean-custom-block clearfix">
    <div class="content">
      <div class="field field-name-field-markup field-type-text-with-summary field-label-hidden">
        <div class="field-items">
          <div class="field-item even">
            <div class="collections-search">
              <h2 class="block__title block-title">Search our collection <span><a href="#">Catalogue</a></span></h2>
              <div id="views-exposed-form">
                <div class="views-exposed-widgets clearfix">
                  <form method="get" action="#" id="searchForm" class="search" _lpchecked="1"><label for="lookfor" class="element-invisible">Search our collection</label><input type="text" id="lookfor" name="lookfor"
                      size="30" placeholder="Find books, maps, audio, databases &amp; more..."> <input type="submit" name="submit" value="Submit search query"></form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
