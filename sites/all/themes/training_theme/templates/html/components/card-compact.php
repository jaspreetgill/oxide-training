<div class="view-mode-compact-wrapper">
  <div class="item-list">
    <ul>
      <li class="views-row views-row-1 views-row-odd views-row-first">
        <div class="ds-1col node node-blog-post view-mode-compact view-mode-compact clearfix ">
          <a href="#" class="group-link field-group-link hasImg">
            <div class="field field-name-field-blog-featured-image field-type-image field-label-hidden">
              <div class="field-items">
                <div class="field-item even"><img src="https://images.unsplash.com/photo-1554296048-b59c9fca4857?ixlib=rb-1.2.1&auto=format&fit=crop&w=1666&q=80" width="220" height="169" alt="" aria-hidden="true"></div>
              </div>
            </div>
            <div class="group-link-content field-group-div" style="">
              <div class="field field-name-post-date field-type-ds field-label-hidden">
                <div class="field-items">
                  <div class="field-item even">04 Apr</div>
                </div>
              </div>
              <div class="field field-name-story-type field-type-ds field-label-hidden">
                <div class="field-items">
                  <div class="field-item even">Blog post</div>
                </div>
              </div>
              <div class="field field-name-title field-type-ds field-label-hidden">
                <div class="field-items">
                  <div class="field-item even">
                    <h3>Compact card title</h3>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>
      </li>
      <li class="views-row views-row-2 views-row-even">
        <div class="ds-1col node node-blog-post node-promoted view-mode-compact view-mode-compact clearfix ">
          <a href="#" class="group-link field-group-link hasImg">
            <div class="field field-name-field-blog-featured-image field-type-image field-label-hidden">
              <div class="field-items">
                <div class="field-item even"><img src="https://images.unsplash.com/photo-1552688419-9949ce9dd731?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1650&q=80" width="220" height="220" alt="News" title="News" aria-hidden="true"></div>
              </div>
            </div>
            <div class="group-link-content field-group-div" style="">
              <div class="field field-name-post-date field-type-ds field-label-hidden">
                <div class="field-items">
                  <div class="field-item even">03 Apr</div>
                </div>
              </div>
              <div class="field field-name-story-type field-type-ds field-label-hidden">
                <div class="field-items">
                  <div class="field-item even">Blog post</div>
                </div>
              </div>
              <div class="field field-name-title field-type-ds field-label-hidden">
                <div class="field-items">
                  <div class="field-item even">
                    <h3>Compact card title</h3>
                  </div>
                </div>
              </div>
            </div>
          </a></div>
      </li>
      <li class="views-row views-row-3 views-row-odd">
        <div class="ds-1col node node-blog-post view-mode-compact view-mode-compact clearfix ">
          <a href="#" class="group-link field-group-link hasImg">
            <div class="field field-name-field-blog-featured-image field-type-image field-label-hidden">
              <div class="field-items">
                <div class="field-item even"><img src="https://images.unsplash.com/photo-1552301807-3285ca1aa48d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=750&q=80" width="220" height="165" alt="" aria-hidden="true"></div>
              </div>
            </div>
            <div class="group-link-content field-group-div" style="">
              <div class="field field-name-post-date field-type-ds field-label-hidden">
                <div class="field-items">
                  <div class="field-item even">25 Mar</div>
                </div>
              </div>
              <div class="field field-name-story-type field-type-ds field-label-hidden">
                <div class="field-items">
                  <div class="field-item even">Blog post</div>
                </div>
              </div>
              <div class="field field-name-title field-type-ds field-label-hidden">
                <div class="field-items">
                  <div class="field-item even">
                    <h3>Compact card title</h3>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>
      </li>
      <li class="views-row views-row-4 views-row-even views-row-last">
        <div class="ds-1col node node-blog-post view-mode-compact view-mode-compact clearfix ">
          <a href="#" class="group-link field-group-link hasImg">
            <div class="field field-name-field-blog-featured-image field-type-image field-label-hidden">
              <div class="field-items">
                <div class="field-item even"><img src="https://images.unsplash.com/photo-1553526777-5ffa3b3248d8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=668&q=80" width="220" height="173" alt="Sample on page 120, Nihon dento orimoto shusei" title="Sample on page 120, Nihon dento orimoto shusei"
                    aria-hidden="true"></div>
              </div>
            </div>
            <div class="group-link-content field-group-div" style="">
              <div class="field field-name-post-date field-type-ds field-label-hidden">
                <div class="field-items">
                  <div class="field-item even">21 Mar</div>
                </div>
              </div>
              <div class="field field-name-story-type field-type-ds field-label-hidden">
                <div class="field-items">
                  <div class="field-item even">Blog post</div>
                </div>
              </div>
              <div class="field field-name-title field-type-ds field-label-hidden">
                <div class="field-items">
                  <div class="field-item even">
                    <h3>Compact card title</h3>
                  </div>
                </div>
              </div>
            </div>
          </a>
        </div>
      </li>
    </ul>
  </div>
</div>