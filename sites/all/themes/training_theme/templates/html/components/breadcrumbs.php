<nav class="breadcrumb" role="navigation">
  <h2 class="element-invisible">You are here</h2>
  <ol>
    <li><a href="#">Home</a></li>
    <li><a href="#">Page title</a></li>
    <li>Link to current page title</li>
  </ol>
</nav>
