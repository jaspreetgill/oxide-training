<div class="item-list card">
  <ul>
    <li class="views-row views-row-1 views-row-odd views-row-first view-mode-featured-wrapper">
      <div class="ds-1col node node-exhibition view-mode-featured view-mode-featured clearfix ">
        <a href="#" class="group-link field-group-link hasImg">
          <div class="group-link-image field-group-div">
            <div class="field field-name-field-exhibition-feat-images field-type-image field-label-hidden">
              <div class="field-items">
                <div class="field-item even"><img src="https://www.nla.gov.au/sites/default/files/styles/card_image__500x310/public/exhibitions/i_750px360_expgbancon_0.jpg?itok=XDWj4RC8" width="500" height="310" alt="Inked: Australian Cartoons branded banner with the Kangaroo mascot illustration by David Pope"
                    title="Inked: Australian Cartoons" aria-hidden="true"></div>
              </div>
            </div>
          </div>
          <div class="group-link-content field-group-div" style="">
            <div class="field field-name-date-short field-type-ds field-label-hidden">
              <div class="field-items">
                <div class="field-item even">
                  <div class="field field-name-field-exhibition-date field-type-date field-label-hidden">
                    <div class="field-items">
                      <div class="field-item even">
                        <div class="date-display"><span class="day">07</span><span class="month">Mar</span></div> to <div class="date-display"><span class="day">21</span><span class="month">Jul</span></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="field field-name-event-type field-type-ds field-label-hidden">
              <div class="field-items">
                <div class="field-item even">Exhibition</div>
              </div>
            </div>
            <div class="field field-name-title field-type-ds field-label-hidden">
              <div class="field-items">
                <div class="field-item even">
                  <h3>Inked: Australian Cartoons</h3>
                </div>
              </div>
            </div>
            <div class="field field-name-field-exhibition-date field-type-date field-label-hidden">
              <div class="field-items">
                <div class="field-item even"><span class="date-display-single"><span class="date-display-start">10:00 am</span> to <span class="date-display-end">5:00 pm</span></span></div>
              </div>
            </div>
            <div class="field field-name-body field-type-text-with-summary field-label-hidden">
              <div class="field-items">
                <div class="field-item even">
                  <p>From lampooning leadership to laughing in the face of adversity, Inked: Australian Cartoons presents a selection of the best cartoons from the National Library of...</p>
                </div>
              </div>
            </div>
          </div>
        </a>
      </div>
    </li>
  </ul>
</div>
