<div class="mainmenu-wrapper">
  <div class="region region-navigation">
    <div id="block-superfish-1" class="block block-superfish contextual-links-region first last odd">
      <div class="block__content">
        <ul id="superfish-1" class="menu sf-menu sf-main-menu sf-horizontal sf-style-none sf-total-items-4 sf-parent-items-3 sf-single-items-1 superfish-processed sf-js-enabled l_tinynav1 sf-shadow">
          <li id="menu-2846-1" class="first odd sf-item-1 sf-depth-1 sf-total-children-6 sf-parent-children-0 sf-single-children-6 menuparent">
            <a href="/research" class="sf-depth-1 menuparent">Research</a>
            <ul style="visibility: hidden; display: none;">
              <li id="menu-10600-1" class="first odd sf-item-1 sf-depth-2 sf-no-children"><a href="#" class="sf-depth-2">Research overview</a></li>
              <li id="menu-10595-1" class="middle even sf-item-2 sf-depth-2 sf-no-children"><a href="#" title="Family and domestic violence have been high on the agenda for Commonwealth and state and territory governments in recent years and there has been a strong push towards an evidence-based approach."
                  class="sf-depth-2">Family and domestic violence</a></li>
              <li id="menu-10596-1" class="middle odd sf-item-3 sf-depth-2 sf-no-children"><a href="#" title="This research aims to identify critical future policy questions and issues affecting law enforcement, the courts and corrections, to document the nature and extent of the problems identified and to canvass the views of stakeholders in the relevant sectors concerning the problems identified."
                  class="sf-depth-2">Futures of crime and justice</a></li>
              <li id="menu-10597-1" class="middle even sf-item-4 sf-depth-2 sf-no-children"><a href="#" title="This research is intended to examine the intersection between organised crime and volume crime. It is based on the understanding that organised crime does not exist in a vacuum, but rather has implications for non-organised volume crime (such as property crime, violence, fraud etc) in both direct and indirect ways."
                  class="sf-depth-2">Organised crime—volume crime</a></li>
              <li id="menu-10598-1" class="middle odd sf-item-5 sf-depth-2 sf-no-children"><a href="#" title="This research examines prison growth by reviewing the available information, supplemented with additional quantitative analysis of published statistics. It also investigates increases in the imprisonment of women and how this impacts on community-based support services."
                  class="sf-depth-2">Reducing demand for prison</a></li>
              <li id="menu-10594-1" class="last even sf-item-6 sf-depth-2 sf-no-children"><a href="#" title="The AIC administers the Drug Use Monitoring in Australia program, National Homicide Monitoring Program and the National Deaths in Custody Program. In addition to these programs, is the administration of the Fraud Against the Commonwealth survey and identity crime surveys."
                  class="sf-depth-2">Crime and justice statistical monitoring</a></li>
            </ul>
          </li>
          <li id="menu-2939-1" class="middle odd sf-item-3 sf-depth-1 sf-total-children-1 sf-parent-children-1 sf-single-children-0 menuparent"><a href="/library" title="The JV Barry Library is a major criminal justice information resource that supports the information needs of the Institute's research programs and provides services to key stakeholders and other clients."
              class="sf-depth-1 menuparent">Library</a>
            <ul style="visibility: hidden; display: none;">
              <li id="menu-2942-1" class="firstandlast odd sf-item-1 sf-depth-2"><a href="#" class="sf-depth-2">Library services</a></li>
            </ul>
          </li>
          <li id="menu-5389-1" class="last even sf-item-4 sf-depth-1 sf-no-children"><a href="#" class="sf-depth-1">Events</a></li>
        </ul>
        <label for="tinynav1" class="element-invisible">Mobile menu</label>
        <select id="tinynav1" class="tinynav tinynav1">
          <option value="#">Menu</option>
          <option value="/research">Research</option>
          <option value="/research/research-overview">- Research overview</option>
          <option value="/research/family-and-domestic-violence">- Family and domestic violence</option>
          <option value="/research/futures-crime-and-justice">- Futures of crime and justice</option>
          <option value="/research/organised-crime-volume-crime">- Organised crime—volume crime</option>
          <option value="/research/reducing-demand-prison">- Reducing demand for prison</option>
          <option value="/research/crime-and-justice-statistical-monitoring">- Crime and justice statistical monitoring</option>
          <option value="/publications">Publications</option>
          <option value="/publications/latest-publications">- Latest publications</option>
          <option value="/publications/rr">- Research Reports</option>
          <option value="/publications/sb">- Statistical Bulletins</option>
          <option value="/publications/sr">- Statistical Reports</option>
          <option value="/publications/tandi">- Trends &amp; issues in crime and criminal justice</option>
          <option value="/publications/series">- Publications by series</option>
          <option value="/library">Library</option>
          <option value="/library/library-services">- Library services</option>
          <option value="/events">Events</option>
        </select>
      </div>
    </div>
  </div>
  <a href="#" class="search-form__toggle toc-filter-processed">Toggle search</a>
</div>
