<fieldset class="collapse-text-fieldset collapsible form-wrapper collapse-processed">
  <legend>
    <span class="fieldset-legend">
      <a class="fieldset-title"><span class="fieldset-legend-prefix element-invisible">Hide</span> Previous reports</a>
      <span class="summary"></span>
    </span>
  </legend>
  <div class="fieldset-wrapper" style="display: block;">
    <div class="collapse-text-text">
      <p>
        <span class="file-filter">
          <a href="#" class="file-filter-document document-956">Annual report 2015-16</a>
          <span class="file-filter-size">(2.9 MB)</span>
        </span>
      </p>
      <p>
        <span class="file-filter">
          <a href="#" class="file-filter-document document-834">Annual report 2014-15</a>
          <span class="file-filter-size">(2.8 MB)</span>
        </span>
      </p>
      <p>
        <span class="file-filter">
          <a href="#" class="file-filter-document document-835">Annual report 2013-14</a>
          <span class="file-filter-size">(3.5 MB)</span>
        </span>
      </p>
      <p>
        <span class="file-filter">
          <a href="#" class="file-filter-document document-276">Annual report 2013-12</a>
          <span class="file-filter-size">(1.6 MB)</span>
        </span>
      </p>
      <p>
        <span class="file-filter">
          <a href="#" class="file-filter-document document-278">Annual report 2012-11</a>
          <span class="file-filter-size">(1 MB)</span>
        </span>
      </p>
      <p>
        <span class="file-filter">
          <a href="#" class="file-filter-document document-277">Annual report 2011-10</a>
          <span class="file-filter-size">(992 KB)</span>
        </span>
      </p>
    </div>
  </div>
</fieldset>
