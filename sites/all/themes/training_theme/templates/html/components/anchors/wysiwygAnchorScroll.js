// Anchor scroll offset for menu
// Anchor links set in WYSIWYG
$('.field-name-body a[href^="#"]').on('click',function (e) {
  // Prevent default link behaviour
  e.preventDefault();
  // Target is defined in link attribute
  var target = this.hash;
  var $trget = $(target);
  // Get height of navbar, select correct ID for site
  var height = $('#navbar').height();
  // offset top by height of navbar
  var newTop = $trget.offset().top - height;
  // Add scroll to anchor animation
  $('html, body').animate ({
      scrollTop: newTop
  }, 500, 'swing', function () {
      // not needed unless you want to change the url value
      // can cause issues with off set though, find a way to clear hash before new anchor
      // window.location.hash = target;
  });
});
