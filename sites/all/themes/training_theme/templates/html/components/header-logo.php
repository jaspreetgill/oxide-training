<div class="header__name-and-slogan" id="name-and-slogan" role="heading" aria-level="1">
  <a href="#" title="Home" class="header__site-link" rel="home">
    <span class="header__site-name" id="site-name">Oxide Interactive</span>
    <span class="header__site-slogan" id="site-slogan">Pattern library</span>
  </a>  
</div>