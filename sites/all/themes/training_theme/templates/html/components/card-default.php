<div class="item-list card">
  <ul>
    <li class="views-row views-row-1 views-row-odd view-mode-card-wrapper">
      <div class="ds-1col node node-exhibition view-mode-card view-mode-card clearfix ">
        <a href="#" class="group-link field-group-link hasImg">
          <div class="group-link-image field-group-div">
            <div class="field field-name-field-exhibition-feat-images field-type-image field-label-hidden">
              <div class="field-items">
                <div class="field-item even"><img src="https://www.nla.gov.au/sites/default/files/styles/card_image__500x310/public/exhibitions/nla.obj-756520643-c-2.jpg?itok=A45I1oE7" width="500" height="310" alt="" title="Cameleers in Front of the Pyramids of Gizeh 1943"
                    aria-hidden="true"></div>
              </div>
            </div>
            <div class="field field-name-date-short field-type-ds field-label-hidden">
              <div class="field-items">
                <div class="field-item even">
                  <div class="field field-name-field-exhibition-date field-type-date field-label-hidden">
                    <div class="field-items">
                      <div class="field-item even">
                        <div class="date-display"><span class="day">08</span><span class="month">Feb</span></div> to <div class="date-display"><span class="day">25</span><span class="month">Aug</span></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="group-link-content field-group-div" style="height: 153px;">
            <div class="field field-name-event-type field-type-ds field-label-hidden">
              <div class="field-items">
                <div class="field-item even">Exhibition</div>
              </div>
            </div>
            <div class="field field-name-title field-type-ds field-label-hidden">
              <div class="field-items">
                <div class="field-item even">
                  <h3>Pilgrimage: Hurley in the Middle East</h3>
                </div>
              </div>
            </div>
            <div class="field field-name-field-exhibition-date field-type-date field-label-hidden">
              <div class="field-items">
                <div class="field-item even"><span class="date-display-single"><span class="date-display-start">10:00 am</span> to <span class="date-display-end">5:00 pm</span></span></div>
              </div>
            </div>
          </div>
        </a>
      </div>
    </li>
    <li class="views-row views-row-2 views-row-even view-mode-card-wrapper">
      <div class="ds-1col node node-event view-mode-card view-mode-card clearfix ">
        <a href="#" class="group-link field-group-link hasImg">
          <div class="group-link-image field-group-div">
            <div class="field field-name-field-event-alert field-type-list-text field-label-hidden">
              <div class="field-items">
                <div class="field-item even">Booked out</div>
              </div>
            </div>
            <div class="field field-name-field-event-feature-image field-type-image field-label-hidden">
              <div class="field-items">
                <div class="field-item even"><img src="https://www.nla.gov.au/sites/default/files/styles/card_image__500x310/public/poetry_web_banner_750x360.jpg?itok=fKU1sCJB" width="500" height="310" alt="This is Home" aria-hidden="true"></div>
              </div>
            </div>
            <div class="field field-name-date-short field-type-ds field-label-hidden">
              <div class="field-items">
                <div class="field-item even">
                  <div class="field field-name-field-event-date2 field-type-date field-label-hidden">
                    <div class="field-items">
                      <div class="field-item even">
                        <div class="date-display"><span class="day">07</span><span class="month">Apr</span></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="group-link-content field-group-div" style="height: 153px;">
            <div class="field field-name-field-event-eventcategory field-type-taxonomy-term-reference field-label-hidden">
              <div class="field-items">
                <div class="field-item even">Bookshop event</div>
              </div>
            </div>
            <div class="field field-name-title field-type-ds field-label-hidden">
              <div class="field-items">
                <div class="field-item even">
                  <h3>This is Home: Poems for Children</h3>
                </div>
              </div>
            </div>
            <div class="field field-name-field-event-location2 field-type-entityreference field-label-hidden">
              <div class="field-items">
                <div class="field-item even">Foyer</div>
              </div>
            </div>
            <div class="field field-name-field-event-date2 field-type-date field-label-hidden">
              <div class="field-items">
                <div class="field-item even"><span class="date-display-single"><span class="date-display-start">2:00 pm</span> to <span class="date-display-end">3:00 pm</span></span></div>
              </div>
            </div>
          </div>
        </a>
      </div>
    </li>
    <li class="views-row views-row-3 views-row-odd view-mode-card-wrapper">
      <div class="ds-1col node node-event view-mode-card view-mode-card clearfix ">
        <a href="#" class="group-link field-group-link hasImg">
          <div class="group-link-image field-group-div">
            <div class="field field-name-field-event-feature-image field-type-image field-label-hidden">
              <div class="field-items">
                <div class="field-item even"><img src="https://www.nla.gov.au/sites/default/files/styles/card_image__500x310/public/sir_nan_kivell_750x360.jpg?itok=s1-JtoqS" width="500" height="310" alt="James Mortimer, Portrait of Rex Nan Kivell (detail), 1950s, nla.cat-vn584380"
                    aria-hidden="true"></div>
              </div>
            </div>
            <div class="field field-name-date-short field-type-ds field-label-hidden">
              <div class="field-items">
                <div class="field-item even">
                  <div class="field field-name-field-event-date2 field-type-date field-label-hidden">
                    <div class="field-items">
                      <div class="field-item even">
                        <div class="date-display"><span class="day">08</span><span class="month">Apr</span></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="group-link-content field-group-div" style="height: 153px;">
            <div class="field field-name-field-event-eventcategory field-type-taxonomy-term-reference field-label-hidden">
              <div class="field-items">
                <div class="field-item even">Talks / Lecture</div>
              </div>
            </div>
            <div class="field field-name-title field-type-ds field-label-hidden">
              <div class="field-items">
                <div class="field-item even">
                  <h3>Lecture with Nat Williams</h3>
                </div>
              </div>
            </div>
            <div class="field field-name-field-event-location2 field-type-entityreference field-label-hidden">
              <div class="field-items">
                <div class="field-item even">Theatre</div>
              </div>
            </div>
            <div class="field field-name-field-event-date2 field-type-date field-label-hidden">
              <div class="field-items">
                <div class="field-item even"><span class="date-display-single"><span class="date-display-start">6:00 pm</span> to <span class="date-display-end">7:00 pm</span></span></div>
              </div>
            </div>
          </div>
        </a>
      </div>
    </li>
    <li class="views-row views-row-4 views-row-even view-mode-card-wrapper">
      <div class="ds-1col node node-event view-mode-card view-mode-card clearfix ">
        <a href="#" class="group-link field-group-link hasImg">
          <div class="group-link-image field-group-div">
            <div class="field field-name-field-event-feature-image field-type-image field-label-hidden">
              <div class="field-items">
                <div class="field-item even"><img src="https://www.nla.gov.au/sites/default/files/styles/card_image__500x310/public/kasuri_750x360_drupal_2.jpg?itok=GMZjJk2G" width="500" height="310" alt="Kasuri fabric sample from nla.cat-vn6489327"
                    aria-hidden="true"></div>
              </div>
            </div>
            <div class="field field-name-date-short field-type-ds field-label-hidden">
              <div class="field-items">
                <div class="field-item even">
                  <div class="field field-name-field-event-date2 field-type-date field-label-hidden">
                    <div class="field-items">
                      <div class="field-item even">
                        <div class="date-display"><span class="day">09</span><span class="month">Apr</span></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="group-link-content field-group-div" style="height: 153px;">
            <div class="field field-name-field-event-eventcategory field-type-taxonomy-term-reference field-label-hidden">
              <div class="field-items">
                <div class="field-item even">Special event</div>
              </div>
            </div>
            <div class="field field-name-title field-type-ds field-label-hidden">
              <div class="field-items">
                <div class="field-item even">
                  <h3>Asian Textiles</h3>
                </div>
              </div>
            </div>
            <div class="field field-name-field-event-location2 field-type-entityreference field-label-hidden">
              <div class="field-items">
                <div class="field-item even">Foyer</div>
              </div>
            </div>
            <div class="field field-name-field-event-date2 field-type-date field-label-hidden">
              <div class="field-items">
                <div class="field-item even"><span class="date-display-single">6:00 pm</span></div>
              </div>
            </div>
          </div>
        </a>
      </div>
    </li>
    <li class="views-row views-row-5 views-row-odd view-mode-card-wrapper">
      <div class="ds-1col node node-event view-mode-card view-mode-card clearfix ">
        <a href="#" class="group-link field-group-link hasImg">
          <div class="group-link-image field-group-div">
            <div class="field field-name-field-event-alert field-type-list-text field-label-hidden">
              <div class="field-items">
                <div class="field-item even">Booked out</div>
              </div>
            </div>
            <div class="field field-name-field-event-feature-image field-type-image field-label-hidden">
              <div class="field-items">
                <div class="field-item even"><img src="https://www.nla.gov.au/sites/default/files/styles/card_image__500x310/public/nla.obj-223759696-1_edited_0.jpg?itok=Ig52xlQl" width="500" height="310" alt="Portrait of Harcourt Algeranoff as the Astrologer in Le coq d'or, Ballets Russes, ca. 1930s / Maurice Seymour"
                    aria-hidden="true"></div>
              </div>
            </div>
            <div class="field field-name-date-short field-type-ds field-label-hidden">
              <div class="field-items">
                <div class="field-item even">
                  <div class="field field-name-field-event-date2 field-type-date field-label-hidden">
                    <div class="field-items">
                      <div class="field-item even">
                        <div class="date-display"><span class="day">10</span><span class="month">Apr</span></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="group-link-content field-group-div" style="height: 153px;">
            <div class="field field-name-field-event-eventcategory field-type-taxonomy-term-reference field-label-hidden">
              <div class="field-items">
                <div class="field-item even">Learning session</div>
              </div>
            </div>
            <div class="field field-name-title field-type-ds field-label-hidden">
              <div class="field-items">
                <div class="field-item even">
                  <h3>Trove for Creativity &amp; Inspiration</h3>
                </div>
              </div>
            </div>
            <div class="field field-name-field-event-location2 field-type-entityreference field-label-hidden">
              <div class="field-items">
                <div class="field-item even">Online</div>
              </div>
            </div>
            <div class="field field-name-field-event-date2 field-type-date field-label-hidden">
              <div class="field-items">
                <div class="field-item even"><span class="date-display-single">1:00 pm</span></div>
              </div>
            </div>
          </div>
        </a>
      </div>
    </li>
    <li class="views-row views-row-6 views-row-even views-row-last view-mode-card-wrapper">
      <div class="ds-1col node node-event view-mode-card view-mode-card clearfix ">
        <a href="#" class="group-link field-group-link hasImg">
          <div class="group-link-image field-group-div">
            <div class="field field-name-field-event-alert field-type-list-text field-label-hidden">
              <div class="field-items">
                <div class="field-item even">Booked out</div>
              </div>
            </div>
            <div class="field field-name-field-event-feature-image field-type-image field-label-hidden">
              <div class="field-items">
                <div class="field-item even"><img src="https://www.nla.gov.au/sites/default/files/styles/card_image__500x310/public/j0364_sp_a_very_great_city_eflyer.jpg?itok=b04MtONU" width="500" height="310" alt="A Very Great City One Day"
                    aria-hidden="true"></div>
              </div>
            </div>
            <div class="field field-name-date-short field-type-ds field-label-hidden">
              <div class="field-items">
                <div class="field-item even">
                  <div class="field field-name-field-event-date2 field-type-date field-label-hidden">
                    <div class="field-items">
                      <div class="field-item even">
                        <div class="date-display"><span class="day">11</span><span class="month">Apr</span></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="group-link-content field-group-div" style="height: 153px;">
            <div class="field field-name-field-event-eventcategory field-type-taxonomy-term-reference field-label-hidden">
              <div class="field-items">
                <div class="field-item even">Bookshop event</div>
              </div>
            </div>
            <div class="field field-name-title field-type-ds field-label-hidden">
              <div class="field-items">
                <div class="field-item even">
                  <h3>Launch: A Very Great City One Day</h3>
                </div>
              </div>
            </div>
            <div class="field field-name-field-event-location2 field-type-entityreference field-label-hidden">
              <div class="field-items">
                <div class="field-item even">Bookshop</div>
                <div class="field-item odd">Foyer</div>
              </div>
            </div>
            <div class="field field-name-field-event-date2 field-type-date field-label-hidden">
              <div class="field-items">
                <div class="field-item even"><span class="date-display-single"><span class="date-display-start">6:00 pm</span> to <span class="date-display-end">7:30 pm</span></span></div>
              </div>
            </div>
          </div>
        </a>
      </div>
    </li>
  </ul>
</div>
