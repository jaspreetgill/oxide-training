<?php
if ($classes) :
  $classes = ' class="' . $classes . '"';
endif;

// Add a, aria role search if this is the search block.
if ($block_html_id == "block-search-form") :
  $role = ' role="search"';
else :
  $role = '';
endif;
?>

<div id="<?php print $block_html_id; ?>" <?php print $classes . $attributes . $role; ?>>
    <?php print render($title_prefix); ?>
    <?php if ($block->subject): ?>
      <h2<?php print $title_attributes; ?>><?php print $block->subject ?></h2>
    <?php endif;?>
  <?php print render($title_suffix); ?>
  <div class="content">
  	<?php print $content ?>
  </div>
</div>
