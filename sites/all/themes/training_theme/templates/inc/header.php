<header role="banner">

  <div class="header">
    <div class="container">
      <?php if ($logo || $site_name || $site_slogan): ?>
      <div class="branding">
        <?php if ($logo): ?>
          <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo">
            <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
          </a>
        <?php endif; ?>
        
      <?php endif; ?>
      </div>

      <?php if ($page['header']) : ?>
        <div class="header-region">
          <?php print render($page['header']); ?>
        </div>
      <?php endif; ?>
    </div>
  </div>

  <?php if ($page['primary_navigation']): ?>
    <nav class="primary-navigation">
      <div class="container">
      <?php print render($page['primary_navigation']); ?>
      </div>
    </nav>
  <?php endif; ?>

</header>
