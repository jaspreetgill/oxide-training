<?php if ($page['footer']) : ?>
  <footer role="contentinfo">
    <div class="container">
      <?php print render($page['footer']); ?>
    </div>
  </footer>
<?php endif; ?>