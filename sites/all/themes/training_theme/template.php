<?php

/**
 * Implements hook_preprocess_html().
 */

function oxide_pure_preprocess_html(&$variables) {
  // Check domain and add env class on body

  if ('127.0.0.1' == $_SERVER["REMOTE_ADDR"]) {
    $variables['classes_array'][] = 'dev';
  } elseif (strstr($_SERVER['HTTP_HOST'], '.oxide.co')) {
    $variables['classes_array'][] = 'preprod';
  }

  // Classes for body element. Allows advanced theming based on context
  if (!$variables['is_front']) {
    /*
    // Add unique class for each page.
    $path = drupal_get_path_alias($_GET['q']);
    // Add unique class for each website section.
    list($section, ) = explode('/', $path, 2);
    $arg = explode('/', $_GET['q']);
    if ($arg[0] == 'node' && isset($arg[1])) {
      if ($arg[1] == 'add') {
        $section = 'node-add';
      }
      elseif (isset($arg[2]) && is_numeric($arg[1]) && ($arg[2] == 'edit' || $arg[2] == 'delete')) {
        $section = 'node-' . $arg[2];
      }
    }
    $variables['classes_array'][] = drupal_html_class('section-' . $section);
    */
  }
}

/**
 * Implements hook_preprocess_page().
 */
function oxide_pure_preprocess_page(&$variables) {

  // Make logo SVG
  // Don't forget to add a copy in PNG
  if (isset($variables['logo'])) {
    $logo = pathinfo($variables['logo']);
    // TODO: check if SVG exist
    $variables['logo'] = $logo['dirname'] . '/' . $logo['filename'] . '.svg';
  }

  // Always print the site name and slogan, but if they are toggled off, we'll
  // just hide them visually.
  $variables['hide_site_name']   = theme_get_setting('toggle_name') ? FALSE : TRUE;
  $variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
  if ($variables['hide_site_name']) {
    // If toggle_name is FALSE, the site_name will be empty, so we rebuild it.
    $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
  }
  if ($variables['hide_site_slogan']) {
    // If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.
    $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
  }

  // Page templates per content type
  if (isset($variables['node'])) {
    // If the node type is "blog_foo" the template suggestion will be "page--blog-foo.tpl.php".
    // $variables['theme_hook_suggestions'][] = 'page__'. $variables['node']->type;
  }
}

/**
 * Implements hook_preprocess_node().
 */
function oxide_pure_preprocess_node(&$variables) {
  /*
  // Add css class "node--NODETYPE--VIEWMODE" to nodes
  $variables['classes_array'][] = 'node--' . $variables['type'] . '--' . $variables['view_mode'];

  // Make "node--NODETYPE--VIEWMODE.tpl.php" templates available for nodes
  $variables['theme_hook_suggestions'][] = 'node__' . $variables['type'] . '__' . $variables['view_mode'];
  */

  /*
  // Redirect 'file' content type straight to the attachment. Comment out if you need the user to have access to the page display.
  $node = $variables['node'];
  $nodeID =  $node->nid;
  if ($variables['type'] == "file") {
    global $base_path;
    global $base_root;
    $pathRedirect =  $base_root . $base_path . 'node/' . $nodeID . '/attachment';
    header('Location: '.$pathRedirect);
  }
  */
}

/**
 * Override or insert variables into the block template
 */
function oxide_pure_preprocess_block(&$vars) {
  $vars['title_attributes_array']['class'][] = 'block-title';
  // $block = $vars['block'];
  // if ($block->delta == 'YOURBLOCK') {}

}
