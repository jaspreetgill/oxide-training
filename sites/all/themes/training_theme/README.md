# OXIDE INTERACTIVE PATTERN LIBRARY
# Oxide Interactive 2018

## Features
* Mobile first
* Grid system via [LostGrid](http://lostgrid.org/)
* Media Query via [Rupture Sass](https://github.com/CubaSAN/rupture-sass)
* Some magic mixin from [RucksackCSS](https://www.rucksackcss.org/)
* IE10 and above support

## Installation
1. Run `npm install` in the theme root
2. Run `gulp` in the theme root
3. Style!

## Notes for reusing and maintaining these components
* Make an alias of the .scss file
* Move the alias to the '/scss/components/' file
* Add link to new component.php file in '/html/components.php'

* `/css/` - compiled CSS
* `/html/` - pages for the key pages. see /index.php for the hierarchy
* `/html/includes/` - reused components in the HTML templates
* `/images/` - theme images, different to images within the content
* `/js/main.js` - a few handy jQuery functions used in this prototype
* `/js/jquery.mmenu.js` - a mobile menu plugin, licenced for this prototype
* `/scss/components/` - styling for reused components
* `/scss/pages/` - styling for specific pages or templates (may be extended to types of content/pages in the build)
* `/scss/sections/` - styling for regions that appear on multiple pages
* `/scss/utils/_animations.scss` - animations used across the site
* `/scss/utils/_mixins.scss` - a few handy SASS mixins used in this prototype
* `/scss/utils/_normalize.scss` - CSS reset
* `/scss/utils/_typography.scss` - styling for typography, including responsive type for mobile
* `/scss/utils/_variables.scss` - SASS variables used to keep code cleaner
* `/scss/style.scss` - NOTE: the prototype has been styled with files included in the order specified in style.scss
* `/gulpfile.js` - configuration for gulp, the compiler
