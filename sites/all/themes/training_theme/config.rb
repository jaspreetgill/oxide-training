require 'compass/import-once/activate'
# Require any additional compass plugins here.

# Change this to :production when ready to deploy the CSS to the live server.
environment = :development
# environment = :production

# Location of the theme's resources.
http_path       = "/"
css_dir         = "css"
sass_dir        = "sass"
fonts_dir       = "fonts"
extensions_dir  = "sass-extensions"
images_dir      = "images"
javascripts_dir = "js"


# Require any additional compass plugins installed on your system.
#require 'ninesixty'
#require 'zen-grids'
# require 'breakpoint'
require 'sass-globbing'

# You can select your preferred output style here (can be overridden via the command line):
# output_style = :expanded or :nested or :compact or :compressed
output_style = (environment == :development) ? :expanded : :compressed

# To enable relative paths to assets via compass helper functions. Since Drupal
# themes can be installed in multiple locations, we don't need to worry about
# the absolute path to the theme from the server root.
relative_assets = true

# To disable debugging comments that display the original location of your selectors. Uncomment:
# line_comments = false

# Hide warnings temporarily
# disable_warnings = true

# Generate css map files for development.
sourcemap = (environment == :development) ? true : false;

# If you prefer the indented syntax, you might want to regenerate this
# project again passing --syntax sass, or you can uncomment this:
# preferred_syntax = :sass
# and then run:
# sass-convert -R --from scss --to sass sass scss && rm -rf sass && mv scss sass
